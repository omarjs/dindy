<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fournisseur extends Model
{
    protected $primarykey='id_fournisseur';

    public function articles()
    {

        
            return $this->hasMany('App\Fournisseur', 'id_fournisseur');
      
    }

}
