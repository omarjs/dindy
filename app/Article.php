<?php

namespace App;

use Illuminate\Database\Eloquent\Model;



class Article extends Model
{

    protected $primarykey='id_article';
    public function fournisseur()
    {
       
            return $this->hasOne('App\Fournisseur', 'id_fournisseur');
       
    }
}
