<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use App\Article;
class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles=DB::table('articles')
        ->join('fournisseurs', 'articles.id_fournisseur', '=', 'fournisseurs.id_fournisseur')
        ->select('articles.*', 'fournisseurs.nom')
        ->get();
        return response()->json($articles);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return response()->json("okokokokok create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

     $article=new Article;
     $article->type=$request->type;
     $article->quantite=$request->quantite;
     $article->date=$request->date;
     $article->prix=$request->prix;
     $article->alert=$request->alert;
     $article->id_fournisseur=$request->fournisseur;
     $article->save();
     return response()->json("l'article est bien ajouter");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Article::where('id_article',$request->id)->update(['type'=>$request->type,'quantite'=>$request->quantite,'date'=>$request->date,'prix'=>$request->prix,'alert'=>$request->alert,'id_fournisseur'=>$request->fournisseur]);
        return response()->json("l'article est bien modifie");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
