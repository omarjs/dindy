import React, { Fragment, useEffect, useState } from "react";
import Popup from "reactjs-popup";
import TypeArticle from "./typearticle";
import AjouterFournisseur from "./ajouterfournisseur";
import ReactDatatable from "@ashvin27/react-datatable";
import ModifieArticle from "./modifiearticle";
import MaterialTable from "material-table";
import {
  Row,
  Col,
  Button,
  Card,
  CardHeader,
  CardBody,
  Form,
  FormGroup,
  FormText,
  Label,
  Input
} from "reactstrap";

const AjouterArticle = () => {
  const [typeaticles, SetType] = useState([]);
  const [fournisseurs, setFournisseurs] = useState([]);
  const [articles, SetArticles] = useState([]);
  const [etat, SetEtat] = useState(true);
  useEffect(() => {
    fetch("/typearticle")
      .then(resp => resp.json())
      .then(data => SetType(data));
    fetch("/fournisseur")
      .then(response => response.json())
      .then(data => {
        setFournisseurs(data);
      });
    fetch("/article")
      .then(resp => resp.json())
      .then(data => SetArticles(data));
  }, [etat]);

  /*********************************************************
  methode ajouter article*/
  const handlesubmit = e => {
    e.preventDefault();
    if (e.target.lastClicked === "modifiearticle") {
      fetch(`/article/modifie`, {
        method: "PUT",
        headers: {
          Accept: "application/json, text/plain, */ ",
          "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
          "Content-type": "application/json"
        },
        body: JSON.stringify({
          id: e.target.id,
          type: e.target.elements.utype.value,
          quantite: e.target.elements.uquantite.value,
          alert: e.target.elements.ualert.value,
          fournisseur: e.target.elements.ufournisseur.value,
          date: e.target.elements.udate.value,
          prix: e.target.elements.uprix.value
        })
      })
        .then(response => response.json())
        .then(data => {
          window.alert(data);
          SetEtat(!etat);
        });
    }
    if (e.target.lastClicked === "ajouterarticle") {
      fetch("/article", {
        method: "POST",
        headers: {
          Accept: "application/json, text/plain, */",
          "Content-type": "application/json"
        },
        body: JSON.stringify({
          type: e.target.elements.type.value,
          quantite: e.target.elements.quantite.value,
          alert: e.target.elements.alert.value,
          fournisseur: e.target.elements.fournisseur.value,
          date: e.target.elements.date.value,
          prix: e.target.elements.prix.value
        })
      })
        .then(res => res.json())
        .then(data => window.alert(data));
    }

    if (e.target.lastClicked === "ajoutertype") {
      fetch("/typearticle", {
        method: "POST",
        headers: {
          Accept: "application/json, text/plain, */",
          "Content-type": "application/json"
        },
        body: JSON.stringify({
          type: e.target.elements.typearticle.value
        })
      })
        .then(res => res.json())
        .then(data => {
          window.alert(data);
          SetEtat(!etat);
        });
    }
    if (e.target.lastClicked === "ajouterfournisseur") {
      fetch("/fournisseur", {
        method: "POST",
        headers: {
          Accept: "application/json, text/plain, */",
          "Content-type": "application/json"
        },
        body: JSON.stringify({
          type: event.target.elements.typefournisseur.value,
          nom: event.target.elements.nom.value,
          telephone: event.target.elements.telephone.value,
          adresse: event.target.elements.adressefournisseur.value
        })
      })
        .then(res => res.json())
        .then(data => {
          window.alert(data);
          SetEtat(!etat);
        });
    }
  };

  //****************************************************** */
  //table config //
  let columns = [
    {
      key: "id_article",
      text: "Ref",
      className: "name",
      align: "center",
      sortable: true
    },
    {
      key: "type",
      text: "Type d'article",
      className: "address",
      align: "center",
      sortable: true
    },
    {
      key: "quantite",
      text: "quantité",
      className: "address",
      align: "center",
      sortable: true
    },
    {
      key: "alert",
      text: "stock alert",
      className: "postcode",
      align: "center",
      sortable: true
    },
    {
      key: "date",
      text: "date d'entrée",
      className: "postcode",
      align: "center",
      sortable: true
    },
    {
      key: "nom",
      text: "Fournisseur",
      className: "postcode",
      align: "center",
      sortable: true
    },
    {
      key: "prix",
      text: "Prix" + "(DH)",
      className: "postcode",
      align: "center",
      sortable: true
    },
    {
      key: "action",
      text: "Action",
      className: "action",
      width: 160,
      align: "center",
      sortable: false,
      cell: record => {
        return (
          <Fragment>
            <Popup
              trigger={
                <Button color="link" size="sm">
                  update
                </Button>
              }
              modal
            >
              {close => (
                <div>
                  <a className="close" onClick={close}>
                    &times;
                  </a>
                  <ModifieArticle
                    _formRef={_formRef}
                    fournisseurs={fournisseurs}
                    typeaticles={typeaticles}
                    article={record}
                  />
                </div>
              )}
            </Popup>
          </Fragment>
        );
      }
    }
  ];
  let config = {
    // page_size: 10,
    //length_menu: [10, 20, 50],
    show_pagination: false,
    button: {
      excel: true,
      print: true
    }
  };
  //********************** */
  let _formRef = null;

  return (
    <Fragment>
      <Form
        encType="multipart/form-data"
        className="form-horizontal"
        name="form1"
        innerRef={ref => (_formRef = ref)}
        onSubmit={handlesubmit}
      >
        <div className="animated fadeIn">
          <Row>
            <Col xs="6" md="12">
              <Card>
                <CardHeader>
                  <strong>Ajouter</strong> Nouveau Article
                </CardHeader>

                <CardBody>
                  <FormGroup row>
                    <Col md="2">
                      <Label htmlFor="text-input"> Type D'article:</Label>
                    </Col>
                    <Col xs="12" md="3">
                      <Input
                        class="form-control"
                        type="select"
                        id="text-input"
                        name="type"
                      >
                        {typeaticles.map((t, index) => (
                          <option key={index} value={t.nom}>
                            {t.nom}
                          </option>
                        ))}
                      </Input>
                    </Col>
                    <Col className="pl-1" md="1">
                      <Popup
                        trigger={
                          <Button color="link" size="sm">
                            Nouveau
                          </Button>
                        }
                        modal
                      >
                        {close => (
                          <div>
                            <a className="close" onClick={close}>
                              &times;
                            </a>
                            <TypeArticle _formRef={_formRef} />
                          </div>
                        )}
                      </Popup>
                    </Col>
                    <Col md="2">
                      <Label htmlFor="text-input"> quantité:</Label>
                    </Col>
                    <Col xs="12" md="2">
                      <Input
                        type="number"
                        id="text-input"
                        name="quantite"
                        min="0"
                      />
                    </Col>
                  </FormGroup>

                  <FormGroup row>
                    <Col md="2">
                      <Label htmlFor="text-input"> alert stock:</Label>
                    </Col>
                    <Col xs="12" md="4">
                      <Input
                        type="number"
                        id="text-input"
                        name="alert"
                        min="0"
                      />
                    </Col>
                    <Col md="2">
                      <Label htmlFor="text-input">Fournisseur:</Label>
                    </Col>
                    <Col xs="12" md="3">
                      <Input type="select" id="text-input" name="fournisseur">
                        {fournisseurs.map((f, index) => (
                          <option value={f.id_fournisseur}>{f.nom}</option>
                        ))}
                      </Input>
                    </Col>
                    <Col className="pl-1" md="1">
                      <Popup
                        trigger={
                          <Button color="link" size="sm">
                            Nouveau
                          </Button>
                        }
                        modal
                      >
                        {close => (
                          <div>
                            <a className="close" onClick={close}>
                              &times;
                            </a>
                            <AjouterFournisseur _formRef={_formRef} />
                          </div>
                        )}
                      </Popup>
                    </Col>
                  </FormGroup>

                  <FormGroup row>
                    <Col md="2">
                      <Label htmlFor="text-input"> date d'entrée:</Label>
                    </Col>
                    <Col xs="12" md="4">
                      <Input type="date" id="text-input" name="date" />
                    </Col>
                    <Col md="2">
                      <Label htmlFor="text-input">prix:</Label>
                    </Col>
                    <Col xs="12" md="2">
                      <Input type="number" id="text-input" name="prix" />
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="2"></Col>
                    <Col xs="12" md="4">
                      <Button
                        type="submit"
                        size="sm"
                        color="primary "
                        className="mr-2"
                        onClick={() =>
                          (_formRef.lastClicked = "ajouterarticle")
                        }
                      >
                        <i className="fa fa-dot-circle-o" /> Ajouter
                      </Button>
                    </Col>
                  </FormGroup>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
        <div className="animated fadeIn">
          <Row>
            <Col xs="6" md="12">
              <Card>
                <CardHeader>
                  <strong>Ajouter</strong> Liste des Articles
                </CardHeader>
                <CardBody>
                  <ReactDatatable
                    className="table col text-center"
                    columns={columns}
                    records={articles}
                    config={config}
                  ></ReactDatatable>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      </Form>
    </Fragment>
  );
};
export default AjouterArticle;
