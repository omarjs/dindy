import React from "react";
import {
  Row,
  Col,
  Button,
  Card,
  CardHeader,
  CardBody,
  Form,
  FormGroup,
  FormText,
  Label,
  Input
} from "reactstrap";

const TypeArticle = props => {
  return (
    <div className="animated fadeIn">
      <Card>
        <CardHeader>
          <strong>Ajouter</strong> Nouveau Type D' Article
        </CardHeader>
        <CardBody>
          <FormGroup row>
            <Col md="4">
              <Label htmlFor="text-input">Type d'Article:</Label>
            </Col>
            <Col xs="12" md="4">
              <Input type="text" id="text-input" name="typearticle" />
            </Col>
          </FormGroup>
          <FormGroup row>
            <Col md="4">
              <Button
                type="submit"
                size="sm"
                color="primary "
                className="mr-2"
                onClick={() => (props._formRef.lastClicked = "ajoutertype")}
              >
                ajouter
              </Button>
            </Col>
            <Col xs="12" md="4"></Col>
          </FormGroup>
        </CardBody>
      </Card>
    </div>
  );
};
export default TypeArticle;
