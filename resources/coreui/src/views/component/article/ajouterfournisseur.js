import React from "react";
import {
  Row,
  Col,
  Button,
  Card,
  CardHeader,
  CardBody,
  Form,
  FormGroup,
  FormText,
  Label,
  Input
} from "reactstrap";
const AjouterFournisseur = props => {
  return (
    <Card>
      <CardHeader>
        <i className="fa fa-align-justify" /> Nouveau Fournisseur
      </CardHeader>
      <CardBody>
        <FormGroup row md="2">
          <Col md="4">
            <Label htmlFor="text-input"> Nom:</Label>
          </Col>
          <Col xs="12" md="8">
            <Input
              class="form-control"
              type="text"
              id="text-input"
              name="nom"
              placeholder="nom"
              //   value={ufournisseur.nom}
              // onChange={handleFieldChange}
              //  onChange={handleFieldChange}
            />
            <FormText color="muted" />
          </Col>
        </FormGroup>
        <FormGroup row md="2">
          <Col md="4">
            <Label htmlFor="text-input"> Tel:</Label>
          </Col>
          <Col xs="12" md="8">
            <Input
              class="form-control"
              type="telephone"
              id="text-input"
              name="telephone"
              placeholder="telephone"
              //  value={ufournisseur.telephone}
              // value={uentrepot.numero}
              //  onChange={handleFieldChange}
            />
            <FormText color="muted" />
          </Col>
        </FormGroup>
        <FormGroup row>
          <Col md="4">
            <Label htmlFor="textarea-input">type:</Label>
          </Col>
          <Col xs="12" md="8">
            <Input type="select" id="textarea-input" name="typefournisseur">
              <option value="particulie">particulie</option>
              <option value="societe">sosciete</option>
              <option></option>
            </Input>
          </Col>
        </FormGroup>
        <FormGroup row>
          <Col md="4">
            <Label htmlFor="textarea-input">Adresse:</Label>
          </Col>
          <Col xs="12" md="8">
            <Input
              type="textarea"
              rows="2"
              id="textarea-input"
              name="adressefournisseur"
              // value={ufournisseur.adresse}
              //  onChange={handleFieldChange}
              // value={uentrepot.adresse}
            />
          </Col>
        </FormGroup>
        <FormGroup row>
          <Col xs="12" md="10">
            <Button
              type="submit"
              size="sm"
              color="primary "
              className="mr-2"
              onClick={() =>
                (props._formRef.lastClicked = "ajouterfournisseur")
              }
              /*  onClick={() =>
          (_formRef.lastClicked = "addfournisseur")
        }*/
            >
              <i className="fa fa-dot-circle-o" /> nouveau
            </Button>
          </Col>
        </FormGroup>
      </CardBody>
    </Card>
  );
};
export default AjouterFournisseur;
