import React, { Fragment, useEffect, useState } from "react";
import {
  Row,
  Col,
  Button,
  Card,
  CardHeader,
  CardBody,
  Form,
  FormGroup,
  FormText,
  Label,
  Input
} from "reactstrap";

const ModifieArticle = props => {
  const [article, SetArticle] = useState({
    alert: "",
    created_at: "2019-09-26 12:21:26",
    date: "article.date",
    id_article: "article.id_article",
    id_fournisseur: "article.id_fournisseur",
    nom: " article.nom",
    prix: " article.prix",
    quantite: "200",
    type: " ",
    updated_at: "2019-10-10 09:59:00"
  });
  useEffect(() => {
    SetArticle(props.article);
  }, []);

  const handleFieldChange = e => {
    console.log("okokokokokok");
    console.log(e.target.name);
    console.log(e.target.value);
    if (e.target.name === "uquantite") {
      SetArticle({
        alert: article.alert,
        created_at: "2019-09-26 12:21:26",
        date: article.date,
        id_article: article.id_article,
        id_fournisseur: article.id_fournisseur,
        nom: article.nom,
        prix: article.prix,
        quantite: e.target.value,
        type: article.type,
        updated_at: "2019-10-10 09:59:00"
      });
    }
    if (e.target.name === "uprix") {
      SetArticle({
        alert: article.alert,
        created_at: "2019-09-26 12:21:26",
        date: article.date,
        id_article: article.id_article,
        id_fournisseur: article.id_fournisseur,
        nom: article.nom,
        prix: e.target.value,
        quantite: article.quantite,
        type: article.type,
        updated_at: "2019-10-10 09:59:00"
      });
    }
    if (e.target.name === "ufournisseur") {
      SetArticle({
        alert: article.alert,
        created_at: "2019-09-26 12:21:26",
        date: article.date,
        id_article: article.id_article,
        id_fournisseur: e.target.value,
        nom: article.nom,
        prix: article.prix,
        quantite: article.quantite,
        type: article.type,
        updated_at: "2019-10-10 09:59:00"
      });
    }
    if (e.target.name === "ualert") {
      SetArticle({
        alert: e.target.value,
        created_at: "2019-09-26 12:21:26",
        date: article.date,
        id_article: article.id_article,
        id_fournisseur: article.id_fournisseur,
        nom: article.nom,
        prix: article.prix,
        quantite: article.quantite,
        type: article.type,
        updated_at: "2019-10-10 09:59:00"
      });
    }
    if (e.target.name === "udate") {
      SetArticle({
        alert: article.alert,
        created_at: "2019-09-26 12:21:26",
        date: e.target.value,
        id_article: article.id_article,
        id_fournisseur: article.id_fournisseur,
        nom: article.nom,
        prix: article.prix,
        quantite: article.quantite,
        type: article.type,
        updated_at: "2019-10-10 09:59:00"
      });
    }
    if (e.target.name === "utype") {
      SetArticle({
        alert: article.alert,
        created_at: "2019-09-26 12:21:26",
        date: article.date,
        id_article: article.id_article,
        id_fournisseur: article.id_fournisseur,
        nom: article.nom,
        prix: article.prix,
        quantite: article.quantite,
        type: e.target.value,
        updated_at: "2019-10-10 09:59:00"
      });
    }
  };

  return (
    <Fragment>
      <div className="animated fadeIn">
        <Row>
          <Col xs="6" md="12">
            <Card>
              <CardHeader>
                <strong>modifie</strong> Article
              </CardHeader>

              <CardBody>
                <FormGroup row>
                  <Col md="3">
                    <Label htmlFor="text-input"> Type D'article:</Label>
                  </Col>
                  <Col xs="12" md="3">
                    <Input
                      class="form-control"
                      type="select"
                      id="text-input"
                      name="utype"
                      value={article.type}
                      onChange={handleFieldChange}
                    >
                      {props.typeaticles.map((t, index) => (
                        <option key={index} value={t.nom}>
                          {t.nom}
                        </option>
                      ))}
                    </Input>
                  </Col>

                  <Col md="2">
                    <Label htmlFor="text-input"> quantité:</Label>
                  </Col>
                  <Col xs="12" md="3">
                    <Input
                      type="number"
                      id="text-input"
                      name="uquantite"
                      min="0"
                      value={article.quantite}
                      onChange={handleFieldChange}
                    />
                  </Col>
                </FormGroup>

                <FormGroup row>
                  <Col md="2">
                    <Label htmlFor="text-input"> alert stock:</Label>
                  </Col>
                  <Col xs="12" md="4">
                    <Input
                      type="number"
                      id="text-input"
                      name="ualert"
                      min="0"
                      value={article.alert}
                      onChange={handleFieldChange}
                    />
                  </Col>
                  <Col md="2">
                    <Label htmlFor="text-input">Fournisseur:</Label>
                  </Col>
                  <Col xs="12" md="3">
                    <Input
                      type="select"
                      id="text-input"
                      name="ufournisseur"
                      onChange={handleFieldChange}
                    >
                      {props.fournisseurs.map((f, index) => (
                        <option value={f.id_fournisseur}>{f.nom}</option>
                      ))}
                    </Input>
                  </Col>
                </FormGroup>

                <FormGroup row>
                  <Col md="2">
                    <Label htmlFor="text-input"> date d'entrée:</Label>
                  </Col>
                  <Col xs="12" md="4">
                    <Input
                      type="date"
                      id="text-input"
                      name="udate"
                      value={article.date}
                      onChange={handleFieldChange}
                    />
                  </Col>
                  <Col md="2">
                    <Label htmlFor="text-input">prix:</Label>
                  </Col>
                  <Col xs="12" md="2">
                    <Input
                      type="number"
                      id="text-input"
                      name="uprix"
                      value={article.prix}
                      onChange={handleFieldChange}
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col md="2"></Col>
                  <Col xs="12" md="4">
                    <Button
                      type="submit"
                      size="sm"
                      color="primary "
                      className="mr-2"
                      onClick={() => {
                        props._formRef.lastClicked = "modifiearticle";
                        props._formRef.id = article.id_article;
                      }}
                    >
                      <i className="fa fa-dot-circle-o" /> valider
                    </Button>
                  </Col>
                </FormGroup>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    </Fragment>
  );
};
export default ModifieArticle;
