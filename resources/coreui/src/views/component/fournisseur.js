import React, { useState, useEffect, Fragment } from "react";
import ReactDatatable from "@ashvin27/react-datatable";
import "bootstrap/dist/css/bootstrap.css";
import {
  Badge,
  Card,
  CardHeader,
  CardBody,
  Table,
  Pagination,
  PaginationItem,
  PaginationLink,
  Row,
  Col,
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink,
  Button,
  Form,
  FormGroup,
  FormText,
  Label,
  Input
} from "reactstrap";

const Fournisseur = () => {
  const [etat, setEtat] = useState("add");
  const [test, setTest] = useState(true);
  const [ufournisseur, setufournisseur] = useState({
    id: "",
    type: "",
    nom: "",
    adresse: "",
    telephone: ""
  });
  const [fournisseurs, setFournisseurs] = useState([]);

  /*useEffect(() => {
    fetch("/artocme")

      .then(res => res.json())
      .then(data => console.log(data));
  }, []);*/
  useEffect(() => {
    fetch("/fournisseur")
      .then(response => response.json())
      .then(data => {
        setFournisseurs(data);
      });
  }, [test]);

  const handlesubmit = e => {
    e.preventDefault();

    if (e.target.lastClicked === "addfournisseur") {
      fetch("/fournisseur", {
        method: "POST",
        headers: {
          Accept: "application/json, text/plain, */",
          "Content-type": "application/json"
        },
        body: JSON.stringify({
          type: event.target.elements.type.value,
          nom: event.target.elements.nom.value,
          telephone: event.target.elements.telephone.value,
          adresse: event.target.elements.adresse.value
        })
      })
        .then(res => res.json())
        .then(
          data => {
            window.alert(data);
            setTest(!test);
          } /* setTest(Math.random())*/
        );
    }
    if (e.target.lastClicked === "updatefournisseur") {
      fetch(`/fournisseur/${ufournisseur}`, {
        method: "PUT",
        headers: {
          Accept: "application/json, text/plain, */ ",
          "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
          "Content-type": "application/json"
        },
        body: JSON.stringify({
          id: ufournisseur.id,
          nom: event.target.elements.nom.value,
          telephone: event.target.elements.telephone.value,
          adresse: event.target.elements.adresse.value
        })
      })
        .then(response => response.json())
        .then(data => {
          console.log(data);
        });
    }
  };
  const handleFieldChange = e => {
    setufournisseur({ nom: e.target.value });
  };
  let columns = [
    {
      key: "id_fournisseur",
      text: "id_fournisseur",
      className: "name",
      align: "center",
      sortable: true
    },
    {
      key: "nom",
      text: "nom",
      className: "address",
      align: "center",
      sortable: true
    },
    {
      key: "telephone",
      text: "telephone",
      className: "address",
      align: "center",
      sortable: true
    },
    {
      key: "adresse",
      text: "adresse",
      className: "postcode",
      align: "center",
      sortable: true
    },
    {
      key: "type",
      text: "type",
      className: "postcode",
      align: "center",
      sortable: true
    },
    {
      key: "action",
      text: "Action",
      className: "action",
      width: 160,
      align: "center",
      sortable: false,
      cell: record => {
        return (
          <Fragment>
            <button
              className="btn btn-success btn-sm  mr-2 ml-2"
              onClick={e => {
                e.preventDefault();
                setufournisseur({
                  id: record.id_fournisseur,
                  nom: record.nom,
                  adresse: record.adresse,
                  telephone: record.telephone
                });
                setEtat("update");
              }}
            >
              <i className="fa fa-edit" />
            </button>
          </Fragment>
        );
      }
    }
  ];
  let config = {
    // page_size: 10,
    //length_menu: [10, 20, 50],
    show_pagination: false,
    button: {
      excel: true,
      print: true
    }
  };

  const block = () => {
    if (etat === "add") {
      return (
        <Button
          type="submit"
          size="sm"
          color="primary "
          className="mr-2"
          //   onClick={() => (_formRef.lastClicked = "addentrepot")}
          onClick={() => (_formRef.lastClicked = "addfournisseur")}
        >
          <i className="fa fa-dot-circle-o" /> nouveau
        </Button>
      );
    }
    if (etat === "update") {
      return (
        <React.Fragment>
          <Button
            type="submit"
            size="sm"
            color="primary "
            className="mr-2"
            onClick={() => (_formRef.lastClicked = "updatefournisseur")}
          >
            valider
          </Button>
          <Button
            type="submit"
            size="sm"
            color="secondary "
            className="mr-2"
            onClick={() => {
              setEtat("add");
              setufournisseur({
                id: "",
                nom: "",
                adresse: "",
                telephone: ""
              });
              //setUentrepot({ id: "", numero: "", nom: "", adresse: "" });
            }}
          >
            annuler
          </Button>
        </React.Fragment>
      );
    }
  };
  let _formRef = null;
  return (
    <Form
      encType="multipart/form-data"
      className="form-horizontal"
      name="form1"
      innerRef={ref => (_formRef = ref)}
      onSubmit={handlesubmit}
    >
      <Row>
        <Col xs="12" lg="3">
          <Card>
            <CardHeader>
              <i className="fa fa-align-justify" /> Fournisseur
            </CardHeader>
            <CardBody>
              <FormGroup row md="2">
                <Col md="4">
                  <Label htmlFor="text-input"> Nom:</Label>
                </Col>
                <Col xs="12" md="8">
                  <Input
                    class="form-control"
                    type="text"
                    id="text-input"
                    name="nom"
                    placeholder="nom"
                    value={ufournisseur.nom}
                    onChange={handleFieldChange}
                    //  onChange={handleFieldChange}
                  />
                  <FormText color="muted" />
                </Col>
              </FormGroup>
              <FormGroup row md="2">
                <Col md="4">
                  <Label htmlFor="text-input"> Tel:</Label>
                </Col>
                <Col xs="12" md="8">
                  <Input
                    class="form-control"
                    type="telephone"
                    id="text-input"
                    name="telephone"
                    placeholder="telephone"
                    value={ufournisseur.telephone}
                    // value={uentrepot.numero}
                    //  onChange={handleFieldChange}
                  />
                  <FormText color="muted" />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Col md="4">
                  <Label htmlFor="textarea-input">type:</Label>
                </Col>
                <Col xs="12" md="8">
                  <Input type="select" id="textarea-input" name="type">
                    <option value="particulie">particulie</option>
                    <option value="societe">sosciete</option>
                    <option></option>
                  </Input>
                </Col>
              </FormGroup>
              <FormGroup row>
                <Col md="4">
                  <Label htmlFor="textarea-input">Adresse:</Label>
                </Col>
                <Col xs="12" md="8">
                  <Input
                    type="textarea"
                    rows="2"
                    id="textarea-input"
                    name="adresse"
                    value={ufournisseur.adresse}
                    //  onChange={handleFieldChange}
                    // value={uentrepot.adresse}
                  />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Col xs="12" md="10">
                  {block()}
                </Col>
              </FormGroup>
            </CardBody>
          </Card>
        </Col>

        <Col xs="12" lg="9">
          <Card>
            <CardHeader>
              <i className="fa fa-align-justify" /> liste des fournisseurs
            </CardHeader>
            <CardBody>
              {/* <Table responsive striped>
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>nom</th>
                    <th>telephone</th>
                    <th>Adresse</th>
                    <th>type</th>
                    <th>actiion</th>
                  </tr>
                  {fournisseurs.map((e, index) => (
                    <tr>
                      <td>{e.id_fournisseur}</td>
                      <td>{e.nom}</td>
                      <td>{e.telephone}</td>
                      <td>{e.adresse}</td>
                      <td>{e.type}</td>
                    </tr>
                  ))}
                </thead>
                <tbody></tbody>
                  </Table>*/}
              <ReactDatatable
                className="table col text-center"
                records={fournisseurs}
                columns={columns}
                config={config}
              ></ReactDatatable>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </Form>
  );
};

export default Fournisseur;
