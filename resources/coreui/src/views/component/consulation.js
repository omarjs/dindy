import React, { useState, useEffect, Fragment } from "react";
import Popup from "reactjs-popup";
import Entre from "./entre";
import Sortie from "./sortie";
import ReactDatatable from "@ashvin27/react-datatable";
import {
  Badge,
  Card,
  CardHeader,
  CardBody,
  Table,
  Pagination,
  PaginationItem,
  PaginationLink,
  Row,
  Col,
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink,
  Button,
  Form,
  FormGroup,
  FormText,
  Label,
  Input
} from "reactstrap";

let columns = [
  {
    key: "id_article",
    text: "Ref",
    className: "name",
    align: "center",
    sortable: true
  },
  {
    key: "type",
    text: "Type d'article",
    className: "address",
    align: "center",
    sortable: true
  },
  {
    key: "quantite",
    text: "quantité",
    className: "address",
    align: "center",
    sortable: true
  },
  {
    key: "alert",
    text: "stock alert",
    className: "postcode",
    align: "center",
    sortable: true
  },
  {
    key: "date",
    text: "date d'entrée",
    className: "postcode",
    align: "center",
    sortable: true
  },
  {
    key: "nom",
    text: "Fournisseur",
    className: "postcode",
    align: "center",
    sortable: true
  },
  {
    key: "prix",
    text: "Prix" + "(DH)",
    className: "postcode",
    align: "center",
    sortable: true
  },

  {
    key: "action",
    text: "Action",
    className: "action",
    width: 160,
    align: "center",
    sortable: false,
    cell: record => {
      return (
        <Fragment>
          <Popup
            trigger={
              <button
                className="btn btn-primary btn-sm"
                data-toggle="tooltip"
                data-placement="top"
                title="habibi ya nouri l3in"
              >
                <i className="fa fa-minus fa-lg" />
              </button>
            }
            modal
          >
            {close => (
              <div>
                <a className="close" onClick={close}>
                  &times;
                </a>
                <Sortie article={record} />
              </div>
            )}
          </Popup>

          <Popup
            trigger={
              <button
                className="btn btn-success btn-sm  mr-2 ml-2"
                data-toggle="tooltip"
                data-placement="top"
                title="habibi ya sakine khayallii"
              >
                <i className="fa fa-plus fa-lg" />
              </button>
            }
            modal
          >
            {close => (
              <div>
                <a className="close" onClick={close}>
                  &times;
                </a>
                <Entre article={record} />
              </div>
            )}
          </Popup>
        </Fragment>
      );
    }
  }
];
let config = {
  // page_size: 10,
  //length_menu: [10, 20, 50],
  show_pagination: false,
  button: {
    excel: true,
    print: true
  }
};

const Consulation = () => {
  const [articles, SetArticles] = useState([]);
  useEffect(() => {
    fetch("/article")
      .then(resp => resp.json())
      .then(data => SetArticles(data));
  }, []);
  return (
    <Fragment>
      <FormGroup row>
        <Col md="2">
          <Label htmlFor="text-input">Debut:</Label>
        </Col>
        <Col xs="12" md="3">
          <Input type="date" id="text-input" name="date" />
        </Col>
        <Col md="2">
          <Label htmlFor="text-input">Fin:</Label>
        </Col>
        <Col xs="12" md="3">
          <Input type="date" id="text-input" name="date" />
        </Col>
      </FormGroup>
      <Col xs="12" lg="12">
        <Card>
          <CardHeader>
            <i className="fa fa-align-justify" /> Etat de Stock
          </CardHeader>
          <CardBody>
            {/* <Table responsive striped>
        <thead>
          <tr>
            <th>ID</th>
            <th>nom</th>
            <th>telephone</th>
            <th>Adresse</th>
            <th>type</th>
            <th>actiion</th>
          </tr>
          {fournisseurs.map((e, index) => (
            <tr>
              <td>{e.id_fournisseur}</td>
              <td>{e.nom}</td>
              <td>{e.telephone}</td>
              <td>{e.adresse}</td>
              <td>{e.type}</td>
            </tr>
          ))}
        </thead>
        <tbody></tbody>
          </Table>*/}
            <ReactDatatable
              className="table col text-center"
              columns={columns}
              config={config}
              records={articles}
            ></ReactDatatable>
          </CardBody>
        </Card>
      </Col>
    </Fragment>
  );
};
export default Consulation;
