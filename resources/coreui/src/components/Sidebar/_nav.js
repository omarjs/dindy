export default {
  items: [
    {
      name: "Dashboard",
      url: "/dashboard",
      icon: "icon-speedometer",
      badge: {
        variant: "info",
        text: "NEW"
      }
    },
    {
      title: true,
      name: "Gestion de stock",
      wrapper: {
        // optional wrapper object
        element: "", // required valid HTML5 element tag
        attributes: {} // optional valid JS object with JS API naming ex: { className: "my-class", style: { fontFamily: "Verdana" }, id: "my-id"}
      },
      class: "" // optional class names space delimited list for title item ex: "text-center"
    },
    {
      name: "Ajouter article",
      url: "/ajouterarticle",
      icon: "icon-drop"
    },
    {
      name: "Etat de stock",
      url: "/consultation",
      icon: "icon-pencil"
    },
    {
      name: "Fournisseur",
      url: "/fournisseurs",
      icon: "icon-drop"
    },
    {
      name: "Facture",
      url: "/facture",
      icon: "icon-drop"
    }
  ]
};
