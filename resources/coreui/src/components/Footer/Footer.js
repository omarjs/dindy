import React, { Component } from "react";

class Footer extends Component {
  render() {
    return (
      <footer className="app-footer">
        <span></span>
        <span className="ml-auto">Powered by M&O DEVCOMPANY</span>
      </footer>
    );
  }
}

export default Footer;
