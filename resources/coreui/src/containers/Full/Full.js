import React, { Component } from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import { Container } from "reactstrap";
import Header from "../../components/Header/";
import Sidebar from "../../components/Sidebar/";
import Breadcrumb from "../../components/Breadcrumb/";
import Aside from "../../components/Aside/";
import Footer from "../../components/Footer/";
import AjouterArticle from "../../views/component/article/ajouterarticle";
import Facture from "../../views/component/facture";
import Fournisseur from "../../views/component/fournisseur";
import Consultation from "../../views/component/consulation";

class Full extends Component {
  render() {
    return (
      <div className="app">
        <Header />
        <div className="app-body">
          <Sidebar {...this.props} />
          <main className="main">
            <Breadcrumb />
            <Container fluid>
              <Switch>
                <Route
                  path="/ajouterarticle"
                  name="article"
                  component={AjouterArticle}
                ></Route>
                <Route
                  path="/consultation"
                  name="consultation"
                  component={Consultation}
                ></Route>
                <Route
                  path="/fournisseurs"
                  name="fournisseur"
                  component={Fournisseur}
                ></Route>
                <Route
                  path="/facture"
                  name="facture"
                  component={Facture}
                ></Route>
                <Redirect from="/" to="/dashboard" />
              </Switch>
            </Container>
          </main>
          <Aside />
        </div>
        <Footer />
      </div>
    );
  }
}

export default Full;
