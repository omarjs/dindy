<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFournisseursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fournisseurs', function (Blueprint $table) {
            
            $table->timestamps();
            $table->bigIncrements('id_fournisseur');
            $table->string('nom', 100)->nullable()->default('text');
            $table->string('telephone', 100)->nullable()->default('text');
            $table->string('adresse', 100)->nullable()->default('text');
            $table->string('type', 100)->nullable()->default('text');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fournisseurs');
    }
}
