<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            
            $table->bigIncrements('id_article');
            $table->string('type', 100)->nullable()->default('text');
            $table->bigInteger('quantite')->nullable()->default(0);
            $table->date('date');
            $table->double('prix');
            $table->double('alert');
            $table->unsignedBigInteger('id_fournisseur');
            $table->foreign('id_fournisseur')->references('id_fournisseur')->on('fournisseurs');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       
        Schema::dropIfExists('articles');
    }
}
