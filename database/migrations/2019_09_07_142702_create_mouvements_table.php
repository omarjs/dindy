<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMouvementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mouvements', function (Blueprint $table) {
            $table->bigIncrements('id_mouvement');
            $table->string('type', 100)->nullable()->default('text');
            $table->date('date');
            $table->bigInteger('quantite')->nullable()->default(12);
            $table->unsignedbiginteger('id_article');
            $table->foreign('id_article')->references('id_article')->on("articles");
              $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mouvements');
    }
}
